//
//  DataInterfaceProtocol.h
//  FieldEngineerApp
//
//  Created by Charlie Smith on 26/05/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaitLocation.h"

@protocol DataInterfaceProtocol <NSObject>

/*!
 * \returns An array of TaitLocation objects
 */
-(NSArray*)getLocationsList;
@end
