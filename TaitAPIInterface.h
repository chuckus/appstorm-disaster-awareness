//
//  TaitAPIInterface.h
//  FieldEngineerApp
//
//  Created by Charlie Smith on 27/05/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataInterfaceProtocol.h"

@interface TaitAPIInterface : NSObject <DataInterfaceProtocol>

@end
