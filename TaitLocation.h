//
//  TaitLocation.h
//  FieldEngineerApp
//
//  Created by Charlie Smith on 26/05/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps.h>

enum ALARM_STATUS {
    RED = -1,
    DISABLED = 0,
    GREEN = 1,
    ASSIGNED_EXTERNAL = 2,
    ASSIGNED_SELF = 3
};

@interface TaitLocation : NSObject <UITableViewDataSource>
@property (nonatomic, assign) float longitude;
@property (nonatomic, assign) float latitude;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, strong) NSMutableDictionary *alarms;
@property (nonatomic, assign) int status;
@property (nonatomic, weak) GMSMarker *marker;
// DEMO
@property (nonatomic, assign) BOOL alertShown;
@end
