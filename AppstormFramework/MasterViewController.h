//
//  MasterViewController.h
//  AppstormFramework
//
//  Created by Charlie Smith on 13/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSMutableArray *vcArray;
@property (strong, nonatomic) UINavigationController *rootDetailViewController;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) UISplitViewController *rootViewController;
@end
