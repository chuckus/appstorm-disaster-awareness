//
//  MapViewController.m
//  AppstormFramework
//
//  Created by Charlie Smith on 14/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import "MapViewController.h"
#import "AssetLocationTracker.h"

@interface MapViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@end

@implementation MapViewController

@synthesize assetPopoverController = _assetPopoverController;
@synthesize assetTableViewController = _assetTableViewController;
@synthesize assetTrackers = _assetTrackers;
@synthesize rootDetailViewController = _rootDetailViewController;
@synthesize link = _link;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Maps";
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(processLocationUpdateDataNotification:) name:@"locationUpdate" object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    assert(_rootDetailViewController != nil);
    assert(_link != nil);
    _mapView.delegate = self;
    // LA
    self.mapView.region = MKCoordinateRegionMake(CLLocationCoordinate2DMake(34.0522, -118.2428), MKCoordinateSpanMake(0.2, 0.2));
    
	// Do any additional setup after loading the view.
    
    
    // Setup popover view controller
}

- (void)viewDidAppear:(BOOL)animated
{
    // Get last known positon
    [self initMapData];
    [super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id<MKAnnotation>)processLocationUpdateDataNotification:(NSNotification*)notification
{
    // Extract assetId from update
    TaitLocationUpdate *update = [notification object];
    NSString *assetId = [[[NSURL alloc] initWithString:update.asset] lastPathComponent];
    // See if it is a selected asset
    AssetLocationTracker* relevantTracker = [_assetTrackers objectForKey:assetId];
    if (relevantTracker == nil)
    {
//        NSLog(@"assetID %@ not in tracker",assetId);
        return nil;
    }
    MKPointAnnotation *newAnnotation = [[MKPointAnnotation alloc] init];
    newAnnotation.coordinate = CLLocationCoordinate2DMake(update.latitude, update.longitude);
    // Remove existing annotation
    if (relevantTracker.annotation != nil)
    {
        CLLocationCoordinate2D* coordArr = malloc(sizeof(CLLocationCoordinate2D) * 2);
        coordArr[0] = [relevantTracker.annotation coordinate];
        coordArr[1] = newAnnotation.coordinate;
        MKPolyline *line = [MKPolyline polylineWithCoordinates:coordArr count:2];
        [relevantTracker.lines addObject:line];
        [_mapView addOverlay:line];
        assert([_mapView.overlays count] > 0);
        [_mapView removeAnnotation:relevantTracker.annotation];
    }
    newAnnotation.title = [relevantTracker.asset productName];
    newAnnotation.subtitle = assetId;
    relevantTracker.annotation = newAnnotation;
    [_mapView addAnnotation:newAnnotation];
    return newAnnotation;
}

- (void)initMapData
{
    NSLog(@"Initialising Tait Asset data from AssetSelectionView");
    self.assetTrackers = [[NSMutableDictionary alloc] init];
    assert(self.annotationsDelegate != nil);
    NSArray* assets = [self.annotationsDelegate updateAssetsToShow];
    for (TaitAsset *ta in assets)
    {
        NSString *assetNum = [[NSURL URLWithString:ta.url] lastPathComponent];
        NSLog(@"Adding assetId %@ to trackers", assetNum);
        [self.assetTrackers setValue:[[AssetLocationTracker alloc] initWithTaitAsset:ta] forKey:assetNum];
        
        [_link sendTaitLocationOfAssetId:assetNum to:self withSelector:@selector(processLocationUpdateDataNotification:)];
        
    }
}

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

#pragma mark - MKMapViewDelegate methods

- (void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated
{
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation
{
    NSLog(@"Putting new pin now");
    MKPinAnnotationView *view = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"PinView"];
    if (view == nil)
    {
        view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PinView"];
    }
    AssetLocationTracker *relevantTracker = [_assetTrackers objectForKey:annotation.subtitle];
    assert(relevantTracker != nil);
    view.pinColor = relevantTracker.color;
    view.draggable = NO;
    view.canShowCallout = YES;
    view.enabled = YES;
    return view;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id < MKOverlay >)overlay
{
    NSLog(@"Attempting to draw overlay");
    MKOverlayView *view;
    MKPolylineView *lineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    lineView.strokeColor = [UIColor blackColor];
    lineView.fillColor = [UIColor whiteColor];
    lineView.lineWidth = 3;
    view = lineView;
    return view;
    
}
@end
