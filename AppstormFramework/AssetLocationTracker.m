//
//  AssetLocationTracker.m
//  AppstormFramework
//
//  Created by Charlie Smith on 16/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import "AssetLocationTracker.h"
#import <MapKit/MapKit.h>

@implementation AssetLocationTracker

@synthesize locations = _locations;
@synthesize lines = _lines;
@synthesize asset = _asset;
@synthesize color = _color;

- (id)initWithTaitAsset:(TaitAsset*)asset
{
    self = [super init];
    if (self)
    {
        _asset = asset;
        _color = MKPinAnnotationColorGreen;
        _lines = [[NSMutableArray alloc] init];
        
    }
    return self;
}
@end
