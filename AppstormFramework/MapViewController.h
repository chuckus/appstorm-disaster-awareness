//
//  MapViewController.h
//  AppstormFramework
//
//  Created by Charlie Smith on 14/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <TaitLocationUpdate.h>
#import "TaitAPILink.h"

@protocol MapViewAnnotationsDelegate

@required
///
///  @returns List of assetIds to show
///
- (NSArray*)updateAssetsToShow;

@end

@interface MapViewController : UIViewController <UISplitViewControllerDelegate, MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) id<MapViewAnnotationsDelegate> annotationsDelegate;
@property (strong, nonatomic) NSMutableDictionary *assetTrackers;
@property (strong, nonatomic) UIPopoverController *assetPopoverController;
@property (strong, nonatomic) UITableViewController *assetTableViewController;
@property (strong, nonatomic) UINavigationController *rootDetailViewController;
@property (strong, nonatomic) TaitAPILink *link;

///
///  @param     Tait LocationUpdate JSON data
///  @returns   MKPlacemark (Only MKAnnotation compatible with GPS co-ordinates)
///
- (id<MKAnnotation>)processLocationUpdateDataNotification:(NSNotification*)notification;

- (void)initMapData;
@end
