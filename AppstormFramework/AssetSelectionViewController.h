//
//  AssetSelectionViewController.h
//  AppstormFramework
//
//  Created by Charlie Smith on 14/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaitAPILink.h"
#import "MapViewController.h"

@interface AssetSelectionViewController : UITableViewController <MapViewAnnotationsDelegate, UISplitViewControllerDelegate>

@property (nonatomic, strong) TaitAPILink* link;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
// @todo: Find better way to test UIViewController
@property (strong, nonatomic) NSMutableArray *assets;
@property (strong, nonatomic) NSMutableArray *selectedAssets;

-(void)updateTableView:(NSArray*)newData;

@end
