//
//  TaitAPILink.h
//  AppstormFramework
//
//  Created by Charlie Smith on 14/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SRWebSocket.h>

@interface TaitAPILink : NSObject <SRWebSocketDelegate>

@property (nonatomic, strong) SRWebSocket *ws;

- (void) connect;
- (void) disconnect;

- (void)sendTaitAssetsListTo:(id)target withSelector:(SEL)selector;
- (void)sendTaitLocationOfAssetId:(NSString*)assetId to:(id)target withSelector:(SEL)selector;

@end
