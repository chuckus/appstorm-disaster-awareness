//
//  AssetSelectionViewController.m
//  AppstormFramework
//
//  Created by Charlie Smith on 14/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import "AssetSelectionViewController.h"

#import "TaitAsset.h"

@interface AssetSelectionViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@end

@implementation AssetSelectionViewController

@synthesize link = _link;
@synthesize assets = _assets;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Assets";
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    NSAssert(link != nil, @"AssetSelectionViewController should have link when loaded up!");
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"VIEW DID APPEAR FOR ASSETS");
    [_link sendTaitAssetsListTo:self withSelector:@selector(updateTableView:)];
    [super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// @todo: Could be a useful code snippet
- (void)updateTableView:(NSArray*)newData
{
    _assets = [newData copy];
    int assets_len = [_assets count];
    // @todo: Use NSArray instead for _selectedAssets
    if (_selectedAssets == nil)
    {
        _selectedAssets = [[NSMutableArray alloc] initWithCapacity:assets_len];
        for (int i = 0; i < assets_len; i++)
        {
            [_selectedAssets addObject:[NSNumber numberWithBool:NO]];
        }
    }
    // Should only change in size if assets added, and that will
    // be to the end of the array so no persistant data
    else
    {
        int selected_len = [_selectedAssets count];
        if (selected_len < assets_len)
        {
            for (int i = 0; i < assets_len - selected_len; i++)
            {
                [_selectedAssets addObject:[NSNumber numberWithBool:NO]];
            }
        }
        else if (assets_len < selected_len)
        {
            for (int i = selected_len - 1; i < selected_len - assets_len; i--)
            {
                [_selectedAssets removeObjectAtIndex:i];
            }
        }
    }
    [self.tableView reloadData];
}

- (NSArray*)updateAssetsToShow
{
    NSMutableArray *selectedTaitAssets = [[NSMutableArray alloc] init];
    assert([_assets count] == [_selectedAssets count]);
    for (int i = 0; [_assets count] > i; i++)
    {
        NSNumber* cur = [_selectedAssets objectAtIndex:i];
        if ([cur boolValue])
        {
            NSLog(@"AssetVieController - assetId %i is selected", i);
            TaitAsset *ta = [_assets objectAtIndex:i];
            /// @todo: Assert types
            [selectedTaitAssets addObject:ta];
        }
    }
    return [NSArray arrayWithArray:[selectedTaitAssets copy]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_assets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    TaitAsset *ta = [_assets objectAtIndex:indexPath.row];
    cell.textLabel.text = ta.assetId;
    if ([[_selectedAssets objectAtIndex:indexPath.row] boolValue])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    bool prevVal = [[_selectedAssets objectAtIndex:indexPath.row] boolValue];
    NSNumber *toggledBool = [NSNumber numberWithBool:!prevVal];
    [_selectedAssets replaceObjectAtIndex:indexPath.row
                               withObject:toggledBool];
    NSLog(@"Row %i is now %@",indexPath.row, toggledBool);
    [tableView reloadData];
}

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}
@end
