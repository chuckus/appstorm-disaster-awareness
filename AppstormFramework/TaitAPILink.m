//
//  TaitAPILink.m
//  AppstormFramework
//
//  Created by Charlie Smith on 14/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import "TaitAPILink.h"
#import <RestKit.h>
#import "TaitAsset.h"
#import "TaitLocationUpdate.h"

@interface TaitAPILink ()

@property (strong, nonatomic) RKObjectManager *rkManager;

@end

@implementation TaitAPILink

@synthesize ws;
@synthesize rkManager = _rkManager;

- (void)connect
{
    // Normal
    [self setupRKManager];
    // Websocket
    self.ws.delegate = nil;
    [self.ws close];
    NSString *urlString = @"wss://taitapi-trial.taitradio.com/api/v1/websocket/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    self.ws = [[SRWebSocket alloc] initWithURLRequest:urlRequest];
    self.ws.delegate = self;
    NSLog(@"Attempting to open websocket");
    [self.ws open];
}

- (void)disconnect
{
    [self.ws close];
}
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
//    NSLog(@"Received message, %@", message);
    if ([message length] > 50)
    {
        TaitLocationUpdate *tlc = [[TaitLocationUpdate alloc] initWithWebsocketMessage:message];
        if (tlc != nil)
        {
            NSLog(@"Websocket latitude = %f", tlc.latitude);
            NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
            [center postNotificationName:@"locationUpdate" object:tlc];
        }
    }
}

- (NSString*)convertToJSONStringFrom:(NSDictionary*)dict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{
    NSLog(@"WS opened");
    [webSocket send:[self convertToJSONStringFrom:@{
                   @"command":@"login",
                   @"token":@"b55x462dy56s28wbxxw9yssn"}]];
    [webSocket send:[self convertToJSONStringFrom:@{
                   @"command":@"status",
                   @"reflect":@"your_custom_string_will_be_in_response"}]];
    [webSocket send:[self convertToJSONStringFrom:@{
                   @"command":@"subscribe",
                   @"reflect":@"my_first_subscription_attempt",
                   @"resource":@"https://taitapi-trial.taitradio.com/api/v1/locationUpdates/"}]];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error;
{
    NSLog(@"Error!");
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean;
{
    NSLog(@"Closed!");
}

- (void)sendTaitAssetsListTo:(id)target withSelector:(SEL)selector
{
    // Mapping Tait API places to my own object graph
    NSLog(@"Getting data...");
    NSLog(@"_rkManager = %@",_rkManager);
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[TaitAsset class]];
    // SERVER API : OUR API
    [mapping addAttributeMappingsFromDictionary:@{
     @"url": @"url",
     @"assetId": @"assetId",
     @"urlDerived": @"urlDerived",
     @"assetType": @"assetType",
     @"manufacturer": @"manufacturer",
     @"productName": @"productName",
     }];
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful); // Anything in 2xx
    RKResponseDescriptor *articleDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping pathPattern:nil keyPath:@"results" statusCodes:statusCodes];
    
    RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
    // The entire value at the source key path containing the errors maps to the message
    [errorMapping addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"message"]];
    statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    // Any response in the 4xx status code range with an "errors" key path uses this mapping
    RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping pathPattern:nil keyPath:@"errors" statusCodes:statusCodes];
    // Search parameters

    [_rkManager addResponseDescriptorsFromArray:@[ articleDescriptor, errorDescriptor]];
    
    [_rkManager getObjectsAtPath:@"/api/v1/assets/"
                     parameters:nil
                        success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                            NSLog(@"Success %@",[mappingResult array]);
                            [target performSelector:selector withObject:[mappingResult array]];
                        }
                        failure:^(RKObjectRequestOperation *operation, NSError *error) {
                            NSLog(@"Failure %@",error.description);
                        }
     ];
}

- (void)sendTaitLocationOfAssetId:(NSString*)assetId to:(id)target withSelector:(SEL)selector
{
    // Mapping Tait API places to my own object graph
    NSLog(@"Getting data...");
    NSLog(@"_rkManager = %@",_rkManager);
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[TaitLocationUpdate class]];
    [mapping addAttributeMappingsFromDictionary:@{
     @"longitude": @"longitude",
     @"latitude": @"latitude",
     @"asset": @"asset",
     @"time": @"time",
     @"speed": @"speed",
     @"course": @"course",
     }];
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful); // Anything in 2xx
    RKResponseDescriptor *articleDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping pathPattern:nil keyPath:@"results" statusCodes:statusCodes];
    
    RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
    // The entire value at the source key path containing the errors maps to the message
    [errorMapping addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"message"]];
    statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    // Any response in the 4xx status code range with an "errors" key path uses this mapping
    RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping pathPattern:nil keyPath:@"errors" statusCodes:statusCodes];
    // Search parameters
    
    [_rkManager addResponseDescriptorsFromArray:@[ articleDescriptor, errorDescriptor]];
    
    // Geofencing
//    NSMutableDictionary *searchParameters = [[NSMutableDictionary alloc] init];
//    [searchParameters setObject:[NSNumber numberWithFloat:MAX_LAT] forKey:@"maxLatitude"];
//    [searchParameters setObject:[NSNumber numberWithFloat:MIN_LAT] forKey:@"minLatitude"];
//    [searchParameters setObject:[NSNumber numberWithFloat:MAX_LONG] forKey:@"maxLongitude"];
//    [searchParameters setObject:[NSNumber numberWithFloat:MIN_LONG] forKey:@"minLongitude"];
    
    [_rkManager getObjectsAtPath:@"/api/v1/locationUpdates/"
                     parameters:nil
                        success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                            NSLog(@"Success");
                            TaitLocationUpdate *tlc = [[mappingResult array] objectAtIndex:0];
                            NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
                            [center postNotificationName:@"locationUpdate" object:tlc];
//                            [target performSelector:selector withObject:[[mappingResult array] objectAtIndex:0]];
                        }
                        failure:^(RKObjectRequestOperation *operation, NSError *error) {
                            NSLog(@"Failure %@",error.description);
                        }
     ];
}

NSString *const taitAPIURLString = @"https://taitapi-trial.taitradio.com";

- (void)setupRKManager
{
    NSURL *url = [NSURL URLWithString:taitAPIURLString];
    _rkManager = [RKObjectManager managerWithBaseURL:url];
    _rkManager.HTTPClient.defaultSSLPinningMode = AFSSLPinningModePublicKey;
    [_rkManager.HTTPClient clearAuthorizationHeader];
    [_rkManager.HTTPClient setAuthorizationHeaderWithUsername:@"demo" password:@"123"];
    [_rkManager.HTTPClient setDefaultHeader:@"Accept" value:@"application/json"];
}

@end
