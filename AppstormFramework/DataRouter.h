//
//  DataRouter.h
//  AppstormFramework
//
//  Created by Charlie Smith on 14/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AssetSelectionViewController.h"

@interface DataRouter : NSObject

@property (nonatomic, weak) AssetSelectionViewController *asvc;

- (void)manageMessage:(NSDictionary*)messageDict;

@end
