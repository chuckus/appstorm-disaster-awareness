//
//  AssetLocationTracker.h
//  AppstormFramework
//
//  Created by Charlie Smith on 16/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaitAsset.h"
#import <MapKit/MapKit.h>

@interface AssetLocationTracker : NSObject

///
///  Array of ordered location updates, ordered by earliest
///  point first.
///
@property (strong, nonatomic) NSMutableArray* locations;
// MKPolylines used for delegate method to draw
@property (strong, nonatomic) NSMutableArray* lines;
@property (strong, nonatomic) TaitAsset* asset;
@property (assign, nonatomic) MKPinAnnotationColor color;
@property (strong, nonatomic) id<MKAnnotation> annotation;

- (id)initWithTaitAsset:(TaitAsset*)asset;

@end
