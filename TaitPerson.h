//
//  TaitPerson.h
//  FieldEngineerApp
//
//  Created by Charlie Smith on 28/05/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaitBiolinkUpdate.h"
#import "TaitLocationUpdate.h"
#import <GoogleMaps/GoogleMaps.h>

@interface TaitPerson : NSObject
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *locatableItem;
@property (nonatomic, assign) int employeeId;
@property (nonatomic, copy) NSString *user;
@property (nonatomic, copy) NSDictionary *userDetails;
@property (nonatomic, strong) TaitBiolinkUpdate *lastBiolinkUpdate;
@property (nonatomic, strong) TaitLocationUpdate *lastLocationUpdate;
@property (nonatomic, weak) GMSMarker *marker;
// DEMO
@property (nonatomic, assign) BOOL alertShown;
@end
