//
//  TaitBiolinkUpdate.h
//  FieldEngineerApp
//
//  Created by Charlie Smith on 28/05/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaitBiolinkUpdate : NSObject
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *person;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, assign) int heartRate;
@property (nonatomic, assign) float coreTemperature;
@property (nonatomic, assign) int posture;
@property (nonatomic, assign) float activity;
@end
