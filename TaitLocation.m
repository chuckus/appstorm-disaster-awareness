//
//  TaitLocation.m
//  FieldEngineerApp
//
//  Created by Charlie Smith on 26/05/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import "TaitLocation.h"

const int NUM_OF_OPTIONS = 1;

@implementation TaitLocation

- (id)init
{
    self = [super self];
    if (self)
    {
        self.status = GREEN;
        self.alarms = [[NSMutableDictionary alloc] init];
        [self.alarms setObject:[NSNumber numberWithBool:NO] forKey:@"Overheating"];
        [self.alarms setObject:[NSNumber numberWithBool:NO] forKey:@"VSWR"];
        [self.alarms setObject:[NSNumber numberWithBool:NO] forKey:@"Low Power"];
        self.alertShown = NO;
    }
    return self;
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"Number of alarms to draw as rows = %i",[self.alarms count]);
    return ([self.alarms count]);//DEMO, commented out -> + NUM_OF_OPTIONS);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if ([self.alarms count] > indexPath.row)
    {
        NSArray *alarmNames = [self.alarms allKeys];
        NSArray *alarmStatuses = [self.alarms allValues];
        NSNumber *alarmStatus = [alarmStatuses objectAtIndex:indexPath.row];
        cell.textLabel.text = [alarmNames objectAtIndex:indexPath.row];
        if ([alarmStatus boolValue])
        {
            [cell.contentView setBackgroundColor:[UIColor redColor]];
            [cell.textLabel setBackgroundColor:[UIColor redColor]];
        }
        else
        {
            [cell.contentView setBackgroundColor:[UIColor greenColor]];
            [cell.textLabel setBackgroundColor:[UIColor greenColor]];
        }
    }
    // Assuming we only have one option.
    else
    {
        UIButton* btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btn.frame = CGRectMake(0,0,80,20);
        btn.titleLabel.text = @"Send engineer";
        btn.center = CGPointMake(cell.contentView.bounds.size.width/2,cell.contentView.bounds.size.height/2);
        [cell.contentView addSubview:btn];
    }
        
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.name;
}
@end
