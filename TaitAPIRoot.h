//
//  TaitAPIRoot.h
//  FieldEngineerApp
//
//  Created by Charlie Smith on 27/05/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaitAPIRoot : NSObject
@property (nonatomic, copy) NSString* usersURL;
@property (nonatomic, copy) NSString* radiosURL;
@property (nonatomic, copy) NSString* subscriptionsURL;
@property (nonatomic, copy) NSString* phonesURL;
@property (nonatomic, copy) NSString* meetingsURL;
@property (nonatomic, copy) NSString* invitationRemindersURL;
@property (nonatomic, copy) NSString* invitationsURL;
@property (nonatomic, copy) NSString* subscriptionPackagesURL;
@property (nonatomic, copy) NSString* personsURL;
@property (nonatomic, copy) NSString* biolinkUpdatesURL;
@property (nonatomic, copy) NSString* locatableItemsURL;
@property (nonatomic, copy) NSString* placesURL;
@property (nonatomic, copy) NSString* messagesURL;
@property (nonatomic, copy) NSString* locationUpdatesURL;
@end
