//
//  TaitLocationUpdate.m
//  FieldEngineerApp
//
//  Created by Charlie Smith on 28/05/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import "TaitLocationUpdate.h"

@implementation TaitLocationUpdate

-(id)initWithWebsocketMessage:(NSString*)message
{
    self = [super init];
    if (self) {
        NSData* data = [message dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSDictionary *updateDict = [dictionary objectForKey:@"resource"];
        self.longitude = [[updateDict objectForKey:@"longitude"] floatValue];
        if (self.longitude == 0.0)
            return nil;
        self.asset = [updateDict objectForKey:@"asset"];
        self.time = [updateDict objectForKey:@"time"];
        self.latitude = [[updateDict objectForKey:@"latitude"] floatValue];
        self.speed = [[updateDict objectForKey:@"speed"] floatValue];
        self.course = [[updateDict objectForKey:@"course"] floatValue];
    }
    return self;
    
}
@end
