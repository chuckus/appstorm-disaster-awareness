import cv2
import cv2.cv as cv_constants

MAX_THRESHOLD = 255
INITIAL_THRESHOLD = 100 

def stream_video_from_file(file_name):
    video_capture = cv2.VideoCapture()
    video_capture.set(cv_constants.CV_CAP_PROP_FOURCC, 
        cv_constants.CV_FOURCC('A', 'V', 'C', '1'))
    video_capture.set(cv_constants.CV_CAP_PROP_FRAME_WIDTH, 1280)
    video_capture.set(cv_constants.CV_CAP_PROP_FRAME_HEIGHT, 720)
    video_capture.open(file_name)

    while 1:
        original_image_window_name = "Original"
        image = video_capture.read()[1]
        cv2.imshow(original_image_window_name, image)
        greyscale = cv2.cvtColor(image, cv_constants.CV_BGR2GRAY)
        blurred_image = cv2.blur(greyscale, (3,3))
        cv2.createTrackbar(
            trackbarname="Canny thresh",
            winname=original_image_window_name,
            value=INITIAL_THRESHOLD,
            count=MAX_THRESHOLD,
            onchange=canny_threshold
            )
        cv2.waitKey(0)

def canny_threshold():
    detected_edges = cv2.GaussianBlur(gray,(3,3),0)
    detected_edges = cv2.Canny(detected_edges,lowThreshold,lowThreshold*ratio,apertureSize = kernel_size)
    dst = cv2.bitwise_and(img,img,mask = detected_edges)  # just add some colours to edges from original image.
    cv2.imshow('canny demo',dst)

def capture_video(cam_num):
    capture = cv.CaptureFromCAM(cam_num)
    writer = cv.CreateVideoWriter(
        filename="output.avi", 
        fourcc=cv.CV_FOURCC('M', 'P', '4', 'V'), 
        fps=30.0, 
        frame_size=(1280,720), 
        is_color=1)
    
    while 1:
        image = cv.QueryFrame(capture)
        cv.WriteFrame(writer, image)
        cv.ShowImage('Image_Window', image)
        cv.WaitKey(2)

if __name__ == "__main__":
    stream_video_from_file(file_name="/Users/smithr/dwhelper/infrared.mp4")
   
