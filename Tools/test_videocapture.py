import cv2.cv as cv

def capture_video(cam_num):
    capture = cv.CaptureFromCAM(cam_num)
    writer = cv.CreateVideoWriter(
        filename="output.avi", 
        fourcc=cv.CV_FOURCC('M', 'P', '4', 'V'), 
        fps=30.0, 
        frame_size=(1280,720), 
        is_color=1)
    
    while 1:
        image = cv.QueryFrame(capture)
        cv.WriteFrame(writer, image)
        cv.ShowImage('Image_Window', image)
        cv.WaitKey(2)

def detect_facial_features(img):
    grayscale = cv.CreateImage(cv.GetSize(img), 8, 1)

if __name__ == "__main__":
    import sys
    cam_num = -1
    if len(sys.argv) > 1:
        cam_num = sys.argv[1]
    capture_video(int(cam_num))
   
