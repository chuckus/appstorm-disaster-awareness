// ==UserScript==
// @name        Journey
// @namespace   taitradio.com
// @include     https://maps.google.co*
// @version     1
// @grant       none
// @require     http://code.jquery.com/jquery-1.9.1.js
// @require 	http://code.jquery.com/ui/1.10.3/jquery-ui.js
// @require  	http://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.js
// ==/UserScript==

console.log("journey")
addGlobalStyle(".ui-timepicker-div .ui-widget-header { margin-bottom: 8px; } \
.ui-timepicker-div dl { text-align: left; } \
.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; } \
.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; } \
.ui-timepicker-div td { font-size: 90%; } \
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; } \
.ui-timepicker-rtl{ direction: rtl; } \
.ui-timepicker-rtl dl { text-align: right; } \
.ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }");
addExternalStyleSheet();

var picker;

window.onerror = ErrorLog;
function ErrorLog (msg, url, line) {
    console.log("error: " + msg + "\n" + "file: " + url + "\n" + "line: " + line);
    return true; // avoid to display an error message in the browser
}

function insertTait() {
        console.log("insertTait")
        var submit = document.getElementById('d_sub')
        var launcher = document.getElementById('d_launcher')
        launcher.style.zIndex = '0';
        var button = document.createElement('button')
        button.className = "kd-button kd-submit-button"
        button.textContent = "POST JOURNEY"
        console.log("button style start")
        button.style.backgroundColor = "rgb(53, 122, 232)";
        button.style.color = "rgb(255, 255, 255)";
        button.style.backgroundImage = "-moz-linear-gradient(center top , rgb(77, 144, 254), rgb(53, 122, 232))"
        button.style.right = '0px';
        button.style.top = '140px';
        button.style.height = '29px';
        button.style.width = '118px';
        button.style.margin = '2px 0px 0px';
        button.style.position = 'absolute';
        console.log("button style end")
        button.onclick = onTait
        submit.parentNode.insertBefore(button, submit.nextSibling)
        inserted = true
        var items;
    	var basic_auth = "Basic " + window.btoa("demo:123");
        $.ajax({
            method: "GET",
            url: 'https://taitapi-trial.taitradio.com/api/v1/assets/',
            headers: { "Authorization": basic_auth, "Accept": "application/json" },
            async: false,
            success: function(data, textStatus, jqXHR) {
                console.log("items = " + data.count);
                items = data.count;
            },
            error: function(xhd, textStatus, errorThrown) {
                console.log()
            }
        });
        select = document.createElement('select')
        for (i = 1; i <= items; i++) {
            var option = document.createElement('option');
            option.text = i;
            option.value = i;
            select.add(option);
        }
        submit.parentNode.style.height = '170px';
        submit.parentNode.insertBefore(select, button);
        console.log("select style end")
        select.style.top = '120px';
        select.style.position = 'absolute';
        console.log("select style end")

        picker = document.createElement('input');
        picker.id = "picker";
	    picker.type = "text";
        //picker.style.visibility = 'hidden';
        submit.parentNode.insertBefore(picker, select);

        $('#picker').datetimepicker({'timeFormat': 'HH:mm:ss'});
        picker.style.height = "16px"
        picker.style.width = "277px"
        picker.style.position = "absolute"
        picker.style.top = "85px"
        //picker.style.margin = "-1px -6px"
        picker.style.padding = "3px 5px"

        var image = document.createElement('img');
        image.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADkAAAAlCAYAAAD4DEFlAAAC50lEQVR42uVZ3XHDIAzOCB2hI2SEjJCXtr7U6XkEj9ANGIER8pi0L4zgEXiIr330CC7iz4CBGNdp7JQ7XQIWQp8kfrVaza0c62J1qhtGrSSyIvTB4oE6tHc8De+3iPLxtTUUN6my+KDu44P+sy+2d2w6njfC0+w3xAP9FwCyHQDyPcq3AJDV/XvyX8zJa62uL6/71ke3xpplxeO2KB4u8T3tivVFYXMFOWmZEiRYlfUtGWFGRNNuf8jyt/chVh+iT7LOU4DMdvuC9aEhWQ7RLM83swA5RJDwXF6lygACw0wNMth3LEhYFFi9GQNQUuNbWGYF0ghTLwBjToa9yebp7EHKAUtrvjlhKD2OQ/NzESDloBjmZmxfC3n1piAn39wDob2YLUSttDDHpMfoL8J/fiBhz0sBtTiQkdX1PkBKD7Z3DfLCHojBy2AIuE3EjDJ3kN5DQOgQ/pzn2z/YQkjq2ZWG9r1IqOKIUfBf7JMqclIO6BS2Bbk1IHVCCYPMqxQvXukwAEdK1OnNdBq7UMRCRs9HWH3ZXfIKCw9JPfHgkSDJDVfXMmkMiOGUK9P4LcR/7xwDUh76005V8tmimfCqZRN//phuC0m9sLvWKcNhyAUi70obnHd5pa5e0pCTgbQMLcb3OYnGVvxRRRzS2YITecP5X+VUl/xR+FL5rNc6xyEencuRr9bnDSf1Ug2CVf34/ai/Q7v6ZvJxknzQxvuZPOyby8tTAudDbzxXH50LYX2AH/op+SE9ephONbJyDvG6SWUw1Sae+ludqepnoqhu6+c/SumxRsonlgzxa+ZCUC9lIGSIcbgeohFpT5h1lYvQlrSUIoZwpDNPpkJ9kEgboGsTMkzvCBDU+t6l9oihB/K0KdmNHlMIk2EgPnZ13yBd6sxnZRukPaj4dXlgLNVXj89BYmt8NTc1PxiL/28MPTZOlEndlWUFYbvOhPS9MgCkk5mKgeyip7WiQrW5Hhd17EwN4hlfY/gBQi7ovQxPEFIAAAAASUVORK5CYII=';
        submit.parentNode.insertBefore(image, picker);
    	console.log("image style")
        image.style.position = "absolute";
        image.style.top = '40px';
}
insertTait();
//launch.onclick = insertTait
var select;

function onTait() {
    var start = document.getElementById('d_d').value
    var end = document.getElementById('d_daddr').value
    console.log(start + " to " + end)
    $.getJSON('http://maps.googleapis.com/maps/api/directions/json?origin=' + start + '&destination=' + end + '&sensor=false',
        function(data) {
            console.log("time: " + $('#picker').datetimepicker('getDate'))
            var startTimeMillis = Date.parse($('#picker').datetimepicker('getDate'));
            console.log("start time millis: " + startTimeMillis)
            var steps = data.routes[0].legs[0].steps
            for (i = 0; i < steps.length; i++) {
                startTimeMillis += steps[i].duration.value * 1000;
                var time = new Date(startTimeMillis).toISOString();
                var basic_auth = "Basic " + window.btoa("demo:123");
                $.ajax({
                    method: "POST",
                    url: "https://taitapi-trial.taitradio.com/api/v1/locationUpdates/",
                    headers: { "Authorization": basic_auth, "Accept": "application/json" },
                    async: false,
                    data: { "asset": "https://taitapi-trial.taitradio.com/api/v1/assets/" + select.options[select.selectedIndex].value + "/",
                            "latitude": steps[i].start_location.lat,
                            "longitude": steps[i].start_location.lng,
                            "time": time,
                           "speed": 4.0,
                           "course":90.0,
                           "accuracy":0.0
                    },
                    success: function(data, textStatus, jqXHR) {
                        console.log(textStatus);
                    },
                    error: function(xhd, textStatus, errorThrown) {
                                            console.log(textStatus);
                    }
                });
              console.log(steps[i].start_location.lat + ", " +  steps[i].start_location.lng + ', ' + time)
            }
        }
    );
    // Enter start time and select locatable item
}

	function addGlobalStyle(css) {
		try {
			var elmHead, elmStyle;
			elmHead = document.getElementsByTagName('head')[0];
			elmStyle = document.createElement('style');
			elmStyle.type = 'text/css';
			elmHead.appendChild(elmStyle);
			elmStyle.innerHTML = css;
		} catch (e) {
			if (!document.styleSheets.length) {
				document.createStyleSheet();
			}
			document.styleSheets[0].cssText += css;
		}
	}

function addExternalStyleSheet() {
		var elmHead = document.getElementsByTagName('head')[0];
		var elmLink = document.createElement('link');
		elmLink.rel = "stylesheet";
		elmLink.href = "http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css";
		elmHead.appendChild(elmLink);
}

