//
//  TaitLocationUpdate.h
//  FieldEngineerApp
//
//  Created by Charlie Smith on 28/05/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaitLocationUpdate : NSObject
@property (nonatomic, assign) float longitude;
@property (nonatomic, assign) float latitude;
@property (nonatomic, copy) NSString* asset;
@property (nonatomic, copy) NSString* time;
@property (nonatomic, assign) float speed;
@property (nonatomic, assign) float course;

-(id)initWithWebsocketMessage:(NSString*)message;
@end
