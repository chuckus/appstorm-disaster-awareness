//
//  TaitAsset.h
//  AppstormFramework
//
//  Created by Charlie Smith on 14/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaitAsset : NSObject

@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *assetId;
@property (nonatomic, copy) NSString *urlDerived;
@property (nonatomic, copy) NSString *assetType;
@property (nonatomic, copy) NSString *manufacturer;
@property (nonatomic, copy) NSString *productName;

@end
