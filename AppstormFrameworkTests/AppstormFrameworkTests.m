//
//  AppstormFrameworkTests.m
//  AppstormFrameworkTests
//
//  Created by Charlie Smith on 13/08/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import "AppstormFrameworkTests.h"

#import "AssetSelectionViewController.h"
#import "TaitLocationUpdate.h"
#import "TaitAsset.h"
#import "AssetLocationTracker.h"

@interface AppstormFrameworkTests ()
@property (nonatomic, strong) UIWindow *window;
@end

@implementation AppstormFrameworkTests

@synthesize window = _window;

- (void)setUp
{
    [super setUp];
    
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testJSONLocationUpdateToNewAnnotation
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"test_LocationUpdateString" ofType:@"txt"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data
                          options:kNilOptions
                          error:&error];
    
    NSLog(@"json: %@", json);
    STAssertNotNil(data, @"Cannot load file");
}

- (void)testUpdateTableView
{
    AssetSelectionViewController *vc = [[AssetSelectionViewController alloc] init];
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:4];
    for (int i = 0; i < 4; i++)
    {
        [arr addObject:[[NSNull alloc] init]];
    }
    [vc updateTableView:arr];
    STAssertNotNil(vc.assets, @"Shouldn't be nil.");
    STAssertEquals((int)[vc.assets count],
                   4,
                   @"Number of rows don't match.");
}

// GIVEN AssetSelectionViewController has a valid selectedAssets and method to get selected assets with names and shit
// WHEN view switches to map view
// THEN we prepare our data
- (void)testProcessSelectedAssetsNew
{
    AssetSelectionViewController *vc = [[AssetSelectionViewController alloc] init];
    TaitAsset *ass1 = [[TaitAsset alloc] init];
    ass1.url = @"https://taitapi-trial.taitradio.com/api/v1/assets/2/";
    ass1.assetId = @"2";
    ass1.productName = @"DMRRadio";
    TaitAsset *ass2 = [[TaitAsset alloc] init];
    ass2.url = @"https://taitapi-trial.taitradio.com/api/v1/assets/3/";
    ass2.assetId = @"3";
    ass2.productName = @"MPTBeam";
    TaitAsset *ass3 = [[TaitAsset alloc] init];
    ass3.url = @"https://taitapi-trial.taitradio.com/api/v1/assets/7/";
    ass3.assetId = @"7";
    ass3.productName = @"P25Repeater";
    vc.assets = [NSArray arrayWithObjects:ass1, ass2, ass3, nil];
    vc.selectedAssets = [NSArray arrayWithObjects:[NSNumber numberWithBool:YES], [NSNumber numberWithBool:YES], [NSNumber numberWithBool:YES], nil];
    MapViewController *mvc = [[MapViewController alloc] init];
    mvc.delegate = vc;
    [mvc initMapData];
    NSDictionary *expected = @{
                           @"2": [[AssetLocationTracker alloc]      initWithTaitAsset:ass1],
                           @"3": [[AssetLocationTracker alloc] initWithTaitAsset:ass2],
                           @"7": [[AssetLocationTracker alloc] initWithTaitAsset:ass3]
                           };
    STAssertTrue([mvc.assetTrackers isEqualToDictionary:expected], [NSString stringWithFormat:@"%@ != %@",mvc.assetTrackers, expected]);
    
    
}

- (void)processFirstLocationForMapView
{
    ///
    ///  1. MapViewController asks TaitAPILink for Locations given from AssetSelectionViewControler.
    ///  2. With data given back, draw the first point.
    ///
    TaitAsset *ass1 = [[TaitAsset alloc] init];
    ass1.url = @"https://taitapi-trial.taitradio.com/api/v1/assets/2/";
    ass1.assetId = @"2";
    ass1.productName = @"DMRRadio";
    TaitAsset *ass2 = [[TaitAsset alloc] init];
    ass2.url = @"https://taitapi-trial.taitradio.com/api/v1/assets/3/";
    ass2.assetId = @"3";
    ass2.productName = @"MPTBeam";
    TaitAsset *ass3 = [[TaitAsset alloc] init];
    ass3.url = @"https://taitapi-trial.taitradio.com/api/v1/assets/7/";
    ass3.assetId = @"7";
    ass3.productName = @"P25Repeater";
    NSDictionary *testDict = @{
                               @"2": [[AssetLocationTracker alloc]      initWithTaitAsset:ass1],
                               @"3": [[AssetLocationTracker alloc] initWithTaitAsset:ass2],
                               @"7": [[AssetLocationTracker alloc] initWithTaitAsset:ass3]
                               };
    
    TaitLocationUpdate *initLocUpdate = [[TaitLocationUpdate alloc] init];
    initLocUpdate.asset = @"https://taitapi-trial.taitradio.com/api/v1/assets/7/";
    initLocUpdate.time = @"2013-08-18T14:20:00Z";
    initLocUpdate.latitude = 34.0622;
    initLocUpdate.longitude = -118.2528;
    
    MapViewController *vc = [[MapViewController alloc] init];
    vc.assetTrackers = [[NSMutableDictionary alloc] initWithDictionary: testDict];
    [vc processLocationUpdateDataNotification:initLocUpdate];
    // MKPinAnnotationView need MKPointAnnotation as self.annotation
    MKPinAnnotationView *anno = (TaitAsset*)[[vc.assetTrackers objectForKey:@"7"] currentAnnotation];
    STAssertEquals(anno., <#a2#>, <#description, ...#>)
}

//// GIVEN we have an existing annotation for an asset (with existing assets)
//// WHEN there is another location update for that particular asset
//// THEN we move the existing annotation for that asset to the particular position.
//
//- (void)processLocationUpdateForArbitraryAsset
//{
//    
//}
@end
